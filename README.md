# Poco

A React Native GitLab client that focus on your issues, merge requests, pipelines
and todos. Possibly more in the future too.

## Screenshots

|Sign In|Merge Requests|MR Detail|MR Drawer|
|-------|--------------|---------|---------|
|![](images/sign-in.png)|![](images/merge-requests.png)|![](images/merge-request-detail.png)|![](images/merge-request-detail-drawer.png)|

## Current Status

This is a **Work in Progress** and does not have much functionality yet. As a
side project, progress is slow because I am working on it as and when I can.

There are no real goals or a strongly defined product roadmap. I'm kinda making
it up as I go along, working on the bits that interest me the most.

**Current Focus:**

Working on the Merge Requests views to give a relatively complete _read only_
experience.

**Problems to Solve:**

- Displaying comments and merge request updates
- Full rendering of GitLab markdown
- More detail on merge request pipelines

## Long Term

It's pretty unlikely this app sees the light of day outside of being a little
side project of mine. However, if I were to seriously consider releasing a full
GitLab client app, features I'd consider adding would be:

- A way to manage merge request process through the app. Not really code reviews,
but replying to comments, modifying the MR, checking pipelines, etc.
- Notifications and highlighting of your own MRs that require your attention. For
example, when a pipeline fails, or there's a conflict, etc.
- Similar features for issues