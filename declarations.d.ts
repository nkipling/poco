declare module '*.svg' {
  import { SvgProps } from 'react-native-svg';
  const content: React.FC<SvgProps>;
  export default content;
}

declare module '@fortawesome/react-native-fontawesome';
declare module 'node-emoji';
declare module 'react-native-dotenv';
declare module 'markdown-it-emoji';
