import React, { useEffect } from 'react';
import { AppState } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Provider, useSelector } from 'react-redux';
import { Route, Router } from 'react-router-native';
import { PersistGate } from 'redux-persist/integration/react';
import axios from './axios';
import { StackNavigation } from './components/routing/stack-navigation';
import history from './history';
import { initIcons } from './icons';
import { persistor, store } from './store';
import { getAuthState, getLoggedIn } from './store/selectors';
import {
  InstanceSettings,
  MainTabsView,
  MergeRequestDetail,
  MoreInformation,
  Welcome,
} from './views';

const App = () => {
  initIcons();

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppRoutes />
      </PersistGate>
    </Provider>
  );
};

const AppRoutes = () => {
  const loggedIn = useSelector(getLoggedIn);
  const authState = useSelector(getAuthState);

  console.disableYellowBox = true;

  useEffect(() => {
    if (loggedIn) {
      // TODO: fix up later
      axios.defaults.headers.common['Authorization'] = `Bearer ${
        authState ? authState.accessToken : ''
      }`;

      // Bypass login screen if we have a session
      history.push('/main-tabs');

      AppState.addEventListener('change', e => {
        if (e === 'active') {
          console.log('App state is active');
        }
      });
    }
  }, []);

  return (
    <Router history={history}>
      <StackNavigation>
        <Route exact path="/" component={Welcome} />
        <Route exact path="/more-information" component={MoreInformation} />
        <Route exact path="/instance-settings" component={InstanceSettings} />
      </StackNavigation>
      <StackNavigation>
        <Route exact path="/main-tabs" component={MainTabsView} />
        <Route exact path="/merge-request" component={MergeRequestDetail} />
      </StackNavigation>
    </Router>
  );
};

export default App;
