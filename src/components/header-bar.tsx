import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useHistory } from 'react-router-native';
import { Brand, Colors } from '../styles';

interface Props {
  title?: string;
  showBack?: boolean;
  showMenu?: boolean;
  showSettings?: boolean;
  rightIcon?: string;
  rightAction?: () => void;
  settingsPress?: () => void;
  menuFunction?: () => void;
}

export const HeaderBar: React.FC<Props> = props => {
  const history = useHistory();

  const onBackPress = () => {
    history.goBack();
  };

  return (
    <View style={styles.headerBar}>
      {props.showBack && (
        <TouchableOpacity
          style={{ ...styles.sideContainer, ...styles.menuContainer }}
          onPress={onBackPress}
        >
          <FontAwesomeIcon icon="chevron-left" color={Colors.White} size={20} />
        </TouchableOpacity>
      )}

      {props.showMenu && (
        <TouchableOpacity
          style={{ ...styles.sideContainer, ...styles.menuContainer }}
          onPress={props.menuFunction}
        >
          <FontAwesomeIcon icon="bars" color={Colors.White} size={20} />
        </TouchableOpacity>
      )}

      <Text style={styles.titleText}>{props.title}</Text>

      <View style={styles.sideContainer}>
        {props.showSettings && (
          <TouchableOpacity
            style={{ ...styles.sideContainer, ...styles.menuContainer }}
            onPress={props.settingsPress}
          >
            <FontAwesomeIcon icon="cog" color={Colors.White} size={20} />
          </TouchableOpacity>
        )}

        {props.rightIcon && props.rightAction && (
          <TouchableOpacity
            style={{ ...styles.sideContainer, ...styles.menuContainer }}
            onPress={props.rightAction}
          >
            <FontAwesomeIcon icon={props.rightIcon} color={Colors.White} size={20} />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  menuContainer: {
    height: 40,
  },
  sideContainer: {
    minWidth: 40,
    maxWidth: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerBar: {
    backgroundColor: Brand.PurpleLight,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  titleText: {
    color: Colors.White,
    fontSize: 18,
  },
});
