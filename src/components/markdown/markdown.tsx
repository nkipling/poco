import React, { useMemo } from 'react';
import { View } from 'react-native';
import { parseMarkdown } from '../../markdown/parser';

export const MRMarkdown: React.FC = props => {
  const res = useMemo(() => parseMarkdown(props.children as string), [props.children]);

  return <View style={{ paddingBottom: 16 }}>{res}</View>;
};
