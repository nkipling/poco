import * as nodeEmoji from 'node-emoji';
import React, { ReactNode } from 'react';
import { Text } from 'react-native';
import FitImage from 'react-native-fit-image';
import { getUniqueID, renderRules } from 'react-native-markdown-renderer';
import { Link } from '..';
import { styles } from './styles';

const defaultTextRenderer = renderRules.text;

const GFMRegEx = {
  ISSUE: /#[0-9]{1,}/g,
};

export const rules = {
  image: (node: any) => {
    let uri;

    if (node.attributes?.src.includes('http')) {
      uri = node.attributes.src;
    } else {
      // TODO: Change this domain to the base for the current GitLab instance.
      // That project path tho... (maybe from references?)
      uri = `https://gitlab.com/gitlab-org/gitlab${node.attributes.src}`;
    }

    return (
      <FitImage
        indicator={true}
        source={{ uri }}
        key={getUniqueID()}
        style={{ flex: 1, ...styles.image }}
      />
    );
  },
  emoji: (node: any) => {
    return <Text key={getUniqueID()}>{nodeEmoji.get(node.content).replace(/:/g, '')}</Text>;
  },
  html_block: (node: any) => {
    if (node.content.includes('<!--')) {
      return null;
    }

    return node;
  },
  link: (node: any, children: any, parent: ReactNode, styles: any) => (
    <Link key={getUniqueID()} url={node.attributes.href} style={styles.link}>
      {children}
    </Link>
  ),
  text: (node: any, children: any, parent: ReactNode, styles: any) => {
    // TODO: Re-write this.
    //       This is a basic and bad way of parsing for GFM tokens
    //       We could look at a couple of options:
    //         Build our own parser, tokeniser, etc
    //         Consider creating a plugin for markdownit
    //         Sending to the Gitlab markdown API endpoint and processing the result

    const { content } = node;
    const result = GFMRegEx.ISSUE.exec(content);

    if (result?.length) {
      const [start, end] = content.split(result[0]);

      return (
        <Text key={getUniqueID()}>
          {start}
          <Link url="" style={styles.link}>
            {result[0]}
          </Link>
          {processLinks(end)}
        </Text>
      );
    } else {
      return defaultTextRenderer(node, children, parent, styles);
    }
  },
};

const processLinks = (content: string) => {
  const result = GFMRegEx.ISSUE.exec(content);

  if (result?.length) {
    const [start, end] = content.split(result[0]);

    return (
      <Text key={getUniqueID()}>
        {start}
        <Link url="" style={styles.link}>
          {result[0]}
        </Link>
        {processLinks(end)}
      </Text>
    );
  } else {
    content;
  }
};
