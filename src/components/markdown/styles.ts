import { StyleSheet } from 'react-native';
import { Brand, Colors } from '../../styles';

export const styles = StyleSheet.create({
  heading2: {
    fontSize: 21,
    fontWeight: 'bold',
    marginTop: 24,
  },
  heading3: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 24,
  },
  image: {
    borderWidth: 1,
    borderColor: Colors.BorderColor,
  },
  text: {
    lineHeight: 20,
  },
  link: {
    color: Brand.Link,
  },
  codeInline: {
    backgroundColor: '#fbe5e1',
    color: '#c0341d',
    borderRadius: 4,
  },
  table: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: Colors.TableBorder,
  },
  tableRow: {
    borderColor: Colors.TableBorder,
    borderBottomWidth: 1,
    flexDirection: 'row',
  },
  tableHeaderCell: {
    flex: 1,
    padding: 5,
    backgroundColor: '#fafafa',
  },
});
