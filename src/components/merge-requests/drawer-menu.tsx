import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Avatar, Label } from '..';
import { MergeRequest } from '../../models';
import { Theme } from '../../styles';
import { Button } from '../ui/button';

interface Props {
  mergeRequest: MergeRequest;
  closePress: () => void;
}

export const InnerDrawer: React.FC<Props> = ({ mergeRequest, closePress }) => {
  const { assignees, labels } = mergeRequest;
  const renderTasks =
    mergeRequest.task_completion_status && mergeRequest.task_completion_status.count > 0;

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.header}>
        <Button type="secondaryGhost" title="Close Merge Request" onPress={() => {}} size="sm" />
        <View style={styles.closeButtonContainer}>
          <TouchableOpacity
            style={{ ...styles.closeButtonContainer, ...styles.closeButton }}
            onPress={closePress}
          >
            <FontAwesomeIcon icon="angle-double-right" color={Theme.SecondaryAction} size={20} />
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.container}>
        <View style={{ ...styles.assignees, ...styles.row }}>
          <Text style={styles.headingText}>
            {assignees.length > 1 ? `${assignees.length} Assignees` : 'Assignee'}
          </Text>
          <View style={styles.assigneeAvatars}>
            {assignees.map(x => (
              <Avatar src={{ uri: x.avatar_url }} style={{ marginRight: 8 }} size="md" key={x.id} />
            ))}
          </View>
        </View>

        <View style={{ ...styles.assignees, ...styles.row }}>
          <Text style={styles.headingText}>Milestone</Text>
          <Text style={styles.valueText}>{mergeRequest.milestone?.title ?? 'None set'}</Text>
        </View>

        <View style={{ ...styles.assignees, ...styles.row }}>
          <Text style={styles.headingText}>Labels</Text>
          <View style={styles.labels}>
            {labels.map((x, index) => (
              <Label style={styles.label} key={index}>
                {x}
              </Label>
            ))}
          </View>
        </View>

        {renderTasks && (
          <View style={{ ...styles.assignees, ...styles.row }}>
            <Text style={styles.headingText}>Tasks</Text>
            <Text style={styles.valueText}>
              {mergeRequest.task_completion_status.completed_count} of{' '}
              {mergeRequest.task_completion_status.count} tasks completed
            </Text>
          </View>
        )}

        <View style={{ ...styles.assignees, ...styles.row }}>
          <Text style={styles.headingText}>Discussion</Text>
          <View style={styles.assigneeAvatars}>
            <FontAwesomeIcon
              icon={mergeRequest.discussion_locked ? 'lock' : 'unlock'}
              color={Theme.SecondaryAction}
              size={12}
              style={{ marginRight: 4 }}
            />
            <Text style={styles.valueText}>
              {mergeRequest.discussion_locked ? 'Locked' : 'Unlocked'}
            </Text>
          </View>
        </View>

        <View style={{ ...styles.assignees, ...styles.row }}>
          <Text style={styles.headingText}>Reference</Text>
          <Text style={styles.valueText}>{mergeRequest.references.full}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  row: {
    paddingHorizontal: 8,
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: Theme.LightBorder,
    flex: 0,
  },
  header: {
    height: 60,
    borderBottomWidth: 1,
    borderBottomColor: Theme.LightBorder,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  headingText: {
    color: Theme.Heading2,
    marginBottom: 8,
  },
  valueText: {
    fontWeight: 'bold',
    color: Theme.Heading1,
  },
  assignees: {
    flex: 1,
  },
  assigneeAvatars: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  closeButtonContainer: {
    minWidth: 40,
    maxWidth: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeButton: {
    height: 40,
  },
  labels: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    flexWrap: 'wrap',
  },
  label: {
    marginRight: 4,
    marginBottom: 4,
  },
});
