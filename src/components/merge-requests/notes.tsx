import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Avatar } from '..';
import { Markdown } from '../../markdown/markdown';
import { MergeRequest, MergeRequestNote } from '../../models';
import { Brand, Colors } from '../../styles';

interface Props {
  mergeRequest: MergeRequest;
}

enum NotesFilter {
  ALL = 'all',
  COMMENTS = 'comments',
  HISTORY = 'history',
}

export const MergeRequestNotes: React.FC<Props> = ({ mergeRequest }) => {
  const [filter, setFilter] = useState(NotesFilter.ALL);
  const { notes } = mergeRequest;

  if (!notes) {
    return null;
  }

  const [notesToShow, setNotesToShow] = useState(notes);

  useEffect(() => {
    switch (filter) {
      case NotesFilter.COMMENTS:
        setNotesToShow(notes.filter(x => !x.system));
        break;

      case NotesFilter.HISTORY:
        setNotesToShow(notes.filter(x => x.system));
        break;

      default:
        setNotesToShow(notes);
        break;
    }
  }, [filter]);

  const onFilterChange = (filter: NotesFilter) => {
    setFilter(filter);
  };

  return (
    <View>
      <FlatList
        data={notesToShow}
        keyExtractor={item => item.id.toString()}
        renderItem={({ item }) => mapNotes(item)}
        ListHeaderComponent={<NotesHeader filter={filter} onFilterChange={onFilterChange} />}
      />
    </View>
  );
};

interface NotesHeaderProps {
  filter: NotesFilter;
  onFilterChange: (filter: NotesFilter) => void;
}

const NotesHeader: React.FC<NotesHeaderProps> = ({ filter, onFilterChange }) => {
  const filterClick = (filter: NotesFilter) => {
    onFilterChange(filter);
  };

  return (
    <View style={[styles.controls, { backgroundColor: Colors.White }]}>
      <View style={styles.tabs}>
        <TouchableOpacity
          style={[styles.tab, filter === NotesFilter.ALL ? styles.tabSelected : {}]}
          onPress={() => filterClick(NotesFilter.ALL)}
        >
          <Text style={filter === NotesFilter.ALL ? styles.tabSelectedText : styles.tabText}>
            All
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.tab, filter === NotesFilter.COMMENTS ? styles.tabSelected : {}]}
          onPress={() => filterClick(NotesFilter.COMMENTS)}
        >
          <Text style={filter === NotesFilter.COMMENTS ? styles.tabSelectedText : styles.tabText}>
            Comments
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.tab, filter === NotesFilter.HISTORY ? styles.tabSelected : {}]}
          onPress={() => filterClick(NotesFilter.HISTORY)}
        >
          <Text style={filter === NotesFilter.HISTORY ? styles.tabSelectedText : styles.tabText}>
            History
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapNotes = (note: MergeRequestNote) => {
  console.log('Note:', note);

  if (typeof note.body !== 'string') {
    return null;
  }

  const NoteComponent = note.system ? SystemNote : UserNote;

  return <NoteComponent note={note} />;
};

interface NoteRendererProps {
  note: MergeRequestNote;
}

const SystemNote: React.FC<NoteRendererProps> = ({ note }) => {
  return (
    <View style={styles.noteContainer}>
      <View style={styles.noteIcon}>
        {/* <FontAwesomeIcon icon="pencil-alt" size={16} /> */}
        <Avatar src={{ uri: note.author?.avatar_url }} size="md" style={styles.userNoteAvatar} />
      </View>
      <View style={styles.noteContent}>
        <Markdown styles={systemNoteOverrides}>{note.body}</Markdown>
      </View>
    </View>
  );
};

const UserNote: React.FC<NoteRendererProps> = ({ note }) => {
  return (
    <View>
      <View style={styles.userNoteContainer}>
        <View style={styles.userNoteSummary}>
          <View>
            <Avatar
              src={{ uri: note.author?.avatar_url }}
              size="md"
              style={styles.userNoteAvatar}
            />
          </View>
          <View>
            <Text style={styles.name}>{note.author?.name}</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.username}>
                @{note.author?.username} {moment(note.created_at).fromNow()}
              </Text>
            </View>
          </View>
        </View>
        <Markdown>{note.body}</Markdown>
      </View>
      <View style={styles.userNoteBorder}></View>
    </View>
  );
};

const leftSpacing = 18;
const bottomSpacing = 8;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  controls: {
    borderBottomWidth: 1,
    borderColor: Colors.BorderColor,
    marginBottom: 16,
  },
  tabs: {
    flexDirection: 'row',
  },
  tab: {
    padding: 16,
  },
  tabSelected: {
    borderBottomWidth: 2,
    borderColor: Brand.PurpleLight,
  },
  tabSelectedText: {
    fontWeight: 'bold',
  },
  tabText: {
    color: Colors.DarkGrey,
  },
  noteContainer: {
    borderLeftWidth: 2,
    borderColor: Colors.BorderColor,
    flexDirection: 'row',
    marginLeft: leftSpacing,
    paddingBottom: bottomSpacing,
  },
  noteIcon: {
    position: 'absolute',
    left: -leftSpacing,
    borderWidth: 2,
    borderRadius: 50,
    borderColor: Colors.BorderColor,
    padding: 8,
    width: 36,
    height: 36,
    backgroundColor: Colors.White,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noteContent: {
    // alignSelf: 'flex-start',
    alignItems: 'center',
    paddingTop: 8,
    marginLeft: 24,
  },
  userNoteContainer: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: Colors.BorderColor,
    padding: 16,
  },
  userNoteBorder: {
    borderLeftWidth: 2,
    borderColor: Colors.BorderColor,
    height: bottomSpacing,
    marginLeft: leftSpacing,
  },
  userNoteSummary: {
    flexDirection: 'row',
  },
  userNoteAvatar: {
    borderWidth: 1,
    borderRadius: 50,
    borderColor: Colors.BorderColor,
  },
  name: {
    fontWeight: 'bold',
    marginLeft: 8,
  },
  username: {
    color: Colors.DarkGrey,
    marginTop: 2,
    marginLeft: 8,
  },
  timeAgo: {
    marginTop: 2,
    // marginLeft: 8,
    color: Colors.DarkGrey,
  },
});

const systemNoteOverrides = StyleSheet.create({
  paragraph: {
    marginTop: 0,
    marginBottom: 0,
  },
});
