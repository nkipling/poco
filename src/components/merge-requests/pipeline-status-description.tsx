import React from 'react';
import { Text } from 'react-native';
import { MergeRequestHeadPipeline, MergeRequestPipeline } from '../../models';

interface StatusIconProps {
  pipeline: MergeRequestHeadPipeline | MergeRequestPipeline;
}

export const PipelineStatusDescription: React.FC<StatusIconProps> = ({ pipeline }) => {
  const status = (pipeline as MergeRequestHeadPipeline)?.detailed_status?.group || pipeline.status;
  const label = (pipeline as MergeRequestHeadPipeline)?.detailed_status?.label;
  const labelOrStatus = label || status;
  const description = `Merge request pipeline #${pipeline.id}`;

  switch (labelOrStatus) {
    case 'running':
      return <Text>{`${description} is running`}</Text>;
    case 'pending':
      return <Text>{`${description} is pending`}</Text>;
    case 'success':
      return <Text>{`${description} has passed`}</Text>;
    case 'passed':
      return <Text>{`${description} has passed`}</Text>;
    case 'failed':
      return <Text>{`${description} has failed`}</Text>;
    case 'canceled':
      return <Text>{`${description} was cancelled`}</Text>;
    case 'skipped':
      return <Text>{`${description} was skipped`}</Text>;

    default:
      return <Text>{`${description} ${label}`}</Text>;
  }
};
