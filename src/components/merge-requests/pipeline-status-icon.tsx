import React from 'react';
import StatusCancelledIcon from '../../assets/svgs/status_canceled.svg';
import StatusFailedIcon from '../../assets/svgs/status_failed.svg';
import StatusPendingIcon from '../../assets/svgs/status_pending.svg';
import StatusRunningIcon from '../../assets/svgs/status_running.svg';
import StatusSkippedIcon from '../../assets/svgs/status_skipped.svg';
import StatusSuccessIcon from '../../assets/svgs/status_success.svg';
import StatusWarningIcon from '../../assets/svgs/status_warning.svg';
import { MergeRequestHeadPipeline, MergeRequestPipeline } from '../../models';
import { Colors } from '../../styles';

interface StatusIconProps {
  pipeline: MergeRequestHeadPipeline | MergeRequestPipeline;
}

export const PipelineStatusIcon: React.FC<StatusIconProps> = ({ pipeline }) => {
  const props = {
    width: 22,
    height: 22,
  };

  const status = (pipeline as MergeRequestHeadPipeline)?.detailed_status?.group || pipeline.status;

  switch (status) {
    case 'running':
      return <StatusRunningIcon {...props} fill={Colors.PipelineRunning} />;
    case 'pending':
      return <StatusPendingIcon {...props} fill={Colors.PipelineSkipped} />;
    case 'success':
      return <StatusSuccessIcon {...props} fill={Colors.PipelineSuccess} />;
    case 'success-with-warnings':
      return <StatusWarningIcon {...props} fill={Colors.PipelineWarning} />;
    case 'failed':
      return <StatusFailedIcon {...props} fill={Colors.PipelineFailed} />;
    case 'canceled':
      return <StatusCancelledIcon {...props} fill={Colors.PipelineCancelled} />;
    case 'skipped':
      return <StatusSkippedIcon {...props} fill={Colors.PipelineSkipped} />;

    default:
      return null;
  }
};
