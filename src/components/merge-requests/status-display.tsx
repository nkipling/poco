import React from 'react';
import { StyleSheet, Text, View, ViewStyle } from 'react-native';
import { Brand, Colors } from '../../styles';

interface Props {
  status: string;
  style?: ViewStyle;
}

type Status = {
  color: string;
  text: string;
};

const statusResolver = (status: string) => {
  let color;
  let text;

  switch (status) {
    case 'opened':
      color = Brand.Success;
      text = 'Open';
      break;

    case 'merged':
      color = Brand.Info;
      text = 'Merged';
      break;

    case 'closed':
      color = Brand.Fail;
      text = 'Closed';
      break;
  }

  return {
    color,
    text,
  };
};

export const StatusDisplay: React.FC<Props> = ({ status, style = {} }) => {
  const { color, text } = statusResolver(status);

  return (
    <View style={{ ...styles.viewStyle, ...style, backgroundColor: color }}>
      <Text style={styles.textStyle}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  viewStyle: {
    backgroundColor: Brand.Success,
    borderRadius: 4,
    paddingVertical: 6,
    paddingHorizontal: 8,
  },
  textStyle: {
    color: Colors.White,
  },
});
