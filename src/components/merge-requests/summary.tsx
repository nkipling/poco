import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { MergeRequest, MergeRequestPipeline } from '../../models';
import { fetchMergeRequestDetail } from '../../store/actions';
import { AppState } from '../../store/reducers';
import { getMergeRequestById } from '../../store/selectors';
import { Colors } from '../../styles';
import { LoadingSpinner } from '../ui/loading-spinner';
import { PipelineStatusIcon } from './pipeline-status-icon';

interface Props {
  noBorder?: boolean;
  mr: MergeRequest;
  onPress?: (mergeRequestId: number) => void;
}

export const MergeRequestSummary: React.FC<Props> = ({ noBorder, mr, onPress }) => {
  const dispatch = useDispatch();
  const border = noBorder ? {} : styles.border;
  const mergeRequest = useSelector((state: AppState) => getMergeRequestById(state, mr.iid));
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    // Check if the merge request has any pipeline info. If not, trigger the fetch
    if (!mergeRequest?.pipeline) {
      setIsLoading(true);
      dispatch(fetchMergeRequestDetail(mr.project_id, mr.iid));
    } else {
      setIsLoading(false);
    }
  }, [mergeRequest]);

  const shouldRenderTaskCompletion = Boolean(
    mr.task_completion_status && mr.task_completion_status.count,
  );

  const onPressOfSummary = () => {
    if (onPress && mergeRequest) {
      onPress(mergeRequest.iid);
    }
  };

  return (
    <TouchableOpacity onPress={onPressOfSummary}>
      <View style={{ ...styles.container, ...border }}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{mr.title}</Text>
          <View style={{ height: 20 }}>
            {isLoading ? (
              <LoadingSpinner inline style={{ color: Colors.DarkGrey }} />
            ) : (
              mergeRequest?.pipeline && (
                <PipelineStatusIcon
                  pipeline={
                    mergeRequest?.head_pipeline || (mergeRequest?.pipeline as MergeRequestPipeline)
                  }
                />
              )
            )}
          </View>
        </View>
        <View style={styles.infoRow}>
          <Text style={[styles.subtitle, styles.subtitleLeft]}>
            updated {moment(mr.updated_at).fromNow()}
          </Text>
          <Text style={[styles.subtitle, styles.subtitleRight]}>
            opened {moment(mr.created_at).fromNow()}
          </Text>
        </View>
        <View style={styles.infoRow}>
          {mr.references && (
            <Text numberOfLines={1} style={[styles.subtitle, styles.subtitleLeft]}>
              {mr.references.full}
            </Text>
          )}

          {shouldRenderTaskCompletion && (
            <Text style={[styles.subtitle, styles.subtitleRight]}>
              {mr.task_completion_status.completed_count} of {mr.task_completion_status.count} tasks
              completed
            </Text>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 16,
    marginHorizontal: 8,
  },
  border: {
    paddingBottom: 16,
    borderBottomWidth: 1,
    borderBottomColor: Colors.BorderColor,
  },
  titleContainer: {
    flexDirection: 'row',
    marginBottom: 4,
    justifyContent: 'space-between',
  },
  title: {
    marginRight: 16,
    fontWeight: 'bold',
    fontSize: 14,
    flexShrink: 1,
  },
  infoRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  subtitle: {
    fontSize: 14,
    lineHeight: 24,
    color: Colors.DarkGrey,
  },
  subtitleLeft: {
    paddingRight: 5,
    textAlign: 'left',
    flex: 1,
  },
  subtitleRight: {
    paddingLeft: 5,
    textAlign: 'right',
    flexShrink: 0,
    flexGrow: 0,
  },
});
