import React, { useEffect, useState } from 'react';
import { Animated, Dimensions, StyleSheet, View } from 'react-native';
import { useHistory } from 'react-router-native';

/**
 * Searches through the supplied children to determine if any match the current pathname.
 * @param pathname path for the current active route
 * @param children children components
 */
const isRouteKnown = (pathname: string, children: any) => {
  for (let c of children) {
    if (pathname === c.props.path) {
      return true;
    }
  }

  return false;
};

export const StackNavigation: React.FC = ({ children }) => {
  const { location } = useHistory();
  const [anim] = useState(new Animated.Value(0));
  const [isHandlingRoute, setIsHandlingRoute] = useState(false);
  const [childToRender, setChildToRender] = useState();
  // BaseComponent is always the first child component
  const [baseComponent, ...routeComponents] = children as any[];

  const triggerAnim = (to: number, callback?: () => void) => {
    Animated.timing(anim, {
      toValue: to,
      duration: 300,
      useNativeDriver: true,
    }).start(callback);
  };

  useEffect(() => {
    const { pathname } = location;

    if (isRouteKnown(pathname, children)) {
      setIsHandlingRoute(true);

      if (pathname !== baseComponent.props.path) {
        // We need to render a child and start the transition
        const childView = routeComponents.find(x => x.props.path === pathname);

        if (childView) {
          setChildToRender(screenWithTransition(anim, childView));
          triggerAnim(1);
        } else {
          throw new Error(`Cannot find component for route: ${pathname}`);
        }
      } else {
        // We are navigating back to the baseComponent, so perform the transition then remove the child
        triggerAnim(0, () => setChildToRender(null));
      }
    } else {
      setIsHandlingRoute(false);
    }
  }, [location]);

  // Don't render anything if this instance isn't handling the current route
  if (!isHandlingRoute) {
    return null;
  }

  return (
    <View style={styles.container}>
      {screenWithTransition(anim, baseComponent, true)}
      {childToRender}
    </View>
  );
};

/**
 * Wraps the supplied component inside a transition animation.
 * @param anim Animation value
 * @param component Component to wrap inside the transition
 * @param base Is this acting as the base of the stack
 */
const screenWithTransition = (anim: Animated.Value, component: any, base: boolean = false) => {
  const screenWidth = Dimensions.get('window').width;
  const ComponentToRender = component.props.component;

  return (
    <Animated.View
      style={{
        position: 'absolute',
        top: 0,
        bottom: 0,
        width: '100%',
        backgroundColor: 'white',
        zIndex: base ? 0 : 2,
        transform: [
          {
            translateX: anim.interpolate({
              inputRange: [0, 1],
              outputRange: base ? [0, -100] : [screenWidth, 0],
            }),
          },
        ],
      }}
    >
      <ComponentToRender />
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
  },
});
