import React from 'react';
import {
  Image,
  ImageSourcePropType,
  ImageURISource,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';
import { Colors } from '../../styles';

type Size = 'sm' | 'md' | 'lg' | 'xl';

interface Props {
  src: ImageSourcePropType | null;
  size?: Size;
  style?: ViewStyle;
}

function workOutSize(size: Size | undefined): number[] {
  switch (size) {
    case 'sm':
      return [24, 24];

    case 'lg':
      return [60, 60];

    case 'xl':
      return [80, 80];

    default:
      return [32, 32];
  }
}

export const Avatar: React.FC<Props> = ({ style = {}, ...props }) => {
  const [width, height] = workOutSize(props.size);
  let imageSource: ImageSourcePropType;

  if (!props.src || !(props.src as ImageURISource).uri) {
    imageSource = { uri: 'http://www.gravatar.com/avatar/?d=identicon' };
  } else {
    imageSource = props.src;
  }

  return (
    <View style={{ ...styles.viewStyle, ...style }}>
      <Image source={imageSource} style={{ height, width }} />
    </View>
  );
};

const styles = StyleSheet.create({
  viewStyle: {
    borderRadius: 50,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: Colors.White,
  },
});
