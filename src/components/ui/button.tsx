import React from 'react';
import { ButtonProps, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Brand, Colors } from '../../styles';
import { pickTextColorBasedOnBgColor } from '../../utils/color';

interface Props extends ButtonProps {
  backgroundColor?: string;
  type: 'primary' | 'primaryGhost' | 'secondary' | 'secondaryGhost';
  size?: 'sm' | 'md';
  marginTop?: number;
}

export const Button: React.FC<Props> = ({
  backgroundColor = 'transparent',
  size = 'md',
  marginTop = 0,
  ...props
}) => {
  const type: any = styles[props.type];
  const color =
    props.color ||
    (styles[props.type] as any)?.color ||
    pickTextColorBasedOnBgColor(type.backgroundColor || backgroundColor);

  const sizeStyles = styles[size];

  return (
    <View style={{ ...styles.button, ...type, marginTop }}>
      <TouchableOpacity onPress={props.onPress} style={styles.buttonOpacity}>
        <Text style={{ ...styles.buttonText, color, ...sizeStyles }}>{props.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    borderStyle: 'solid',
    borderWidth: 1,
  },
  buttonOpacity: {
    width: '100%',
  },
  buttonText: {
    textAlign: 'center',
  },
  sm: {
    paddingHorizontal: 8,
    paddingVertical: 8,
    fontSize: 12,
  },
  md: {
    paddingHorizontal: 8,
    paddingVertical: 16,
    fontSize: 16,
  },
  primary: {
    backgroundColor: Brand.PurpleLight,
    borderColor: Brand.PurpleLight,
  },
  primaryGhost: {
    backgroundColor: Colors.White,
    borderColor: Brand.PurpleLight,
    color: Brand.PurpleLight,
  },
  secondary: {
    backgroundColor: Brand.Orange,
    borderColor: Brand.Orange,
    color: Colors.White,
  },
  secondaryGhost: {
    backgroundColor: 'transparent',
    color: Brand.Orange,
    borderColor: Brand.Orange,
  },
});
