import React from 'react';
import { StyleSheet, View } from 'react-native';

export const ContentContainer: React.FC = props => {
  return <View style={styles.container}>{props.children}</View>;
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
});
