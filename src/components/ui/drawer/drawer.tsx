import React, { useEffect, useState } from 'react';
import { Animated, Dimensions, PanResponder, StyleSheet } from 'react-native';
import { Theme } from '../../../styles';
import { FadeIn } from './fade-in';

type Position = 'left' | 'right';

interface Props {
  open: boolean;
  position?: Position;
  onClose?: () => void;
}

const SCREEN_WIDTH = Dimensions.get('window').width;
const MENU_WIDTH = Math.round(SCREEN_WIDTH * 0.7);

export const Drawer: React.FC<Props> = ({ open, position = 'left', onClose, children }) => {
  const [isOpen, setIsOpen] = useState(open);
  const [slideIn] = useState(new Animated.Value(0));
  const reversePosition = position === 'left' ? 'right' : 'left';
  const [backgroundOffset, setBackgroundOffset] = useState(getBackgroundOffset(position));

  let currPosition: number;

  useEffect(() => {
    setIsOpen(open);

    if (open === false) {
      closeDrawer();
    } else {
      setBackgroundOffset(open ? 0 : getBackgroundOffset(position));
    }
  }, [open]);

  useEffect(() => {
    Animated.timing(slideIn, {
      toValue: getAnimationToValue(isOpen, position),
      duration: isOpen ? 300 : 150,
      useNativeDriver: true,
    }).start();
  }, [isOpen]);

  const closeDrawer = () => {
    setIsOpen(false);

    setTimeout(() => {
      setBackgroundOffset(open ? 0 : getBackgroundOffset(position));

      if (onClose) {
        onClose();
      }
    }, 150);
  };

  const closeAfterDismissedByTouch = () => {
    setIsOpen(false);

    if (onClose) {
      onClose();
    }
  };

  // === Gesture Handling === //

  const panResponder = PanResponder.create({
    // // Ask to be the responder:
    onStartShouldSetPanResponder: (evt, gestureState) => true,
    onStartShouldSetPanResponderCapture: (evt, gestureState) => false,

    onMoveShouldSetPanResponderCapture: (evt, gestureState) => {
      // const { dx, dy } = gestureState;
      // return dx > 2 || dx < -2 || dy > 2 || dy < -2;

      return !(gestureState.dx === 0 && gestureState.dy === 0);
    },

    onMoveShouldSetPanResponder: (evt, gestureState) => {
      const { dx, dy } = gestureState;
      return !(dx === 0 && dy === 0);
    },
    onPanResponderMove: (evt, gestureState) => {
      const { dx } = gestureState;

      // Default is position left
      let startingPoint = 0;
      let destinationPoint = 0;

      if (position === 'right') {
        startingPoint = SCREEN_WIDTH - MENU_WIDTH;
      }

      destinationPoint = startingPoint + dx;

      if (position === 'left' && destinationPoint > startingPoint) {
        destinationPoint = startingPoint;
      } else if (position === 'right' && destinationPoint < startingPoint) {
        destinationPoint = startingPoint;
      }

      currPosition = destinationPoint;

      Animated.timing(slideIn, {
        duration: 1,
        toValue: destinationPoint,
        useNativeDriver: true,
      }).start();
    },
    onPanResponderRelease: (evt, gestureState) => {
      // Set the default to keep the drawer open
      let clampTo = SCREEN_WIDTH - MENU_WIDTH;
      const { vx } = gestureState;
      let snapClose = false;

      if (Math.abs(vx) > 0.7) {
        snapClose = true;
      } else if (position === 'left' && currPosition < MENU_WIDTH / 2) {
        snapClose = true;
      } else if (position === 'right' && currPosition > SCREEN_WIDTH - MENU_WIDTH / 2) {
        snapClose = true;
      }

      // Close the drawer if we move past 50%
      if (snapClose) {
        clampTo = position === 'left' ? -MENU_WIDTH : SCREEN_WIDTH;
        currPosition = 0;

        Animated.timing(slideIn, {
          toValue: clampTo,
          duration: 150,
          useNativeDriver: true,
        }).start(() => closeAfterDismissedByTouch());
      } else {
        Animated.spring(slideIn, {
          toValue: clampTo,
          friction: 50,
          useNativeDriver: true,
        }).start();
      }
    },
  });

  return (
    <FadeIn
      style={{ ...styles.container, [reversePosition]: backgroundOffset, width: SCREEN_WIDTH }}
      trigger={isOpen}
    >
      <Animated.ScrollView
        alwaysBounceVertical={false}
        style={{
          ...styles.drawer,
          transform: [
            {
              translateX: slideIn,
            },
          ],
        }}
        {...panResponder.panHandlers}
      >
        {children}
      </Animated.ScrollView>
    </FadeIn>
  );
};

/**
 * Helper that returns the offset for the background based on drawer position.
 * @param position
 */
const getBackgroundOffset = (position: Position) => {
  return position === 'left' ? SCREEN_WIDTH * 2 : SCREEN_WIDTH;
};

/**
 * Helper that returns the correct animation to value for the drawer slide in.
 * @param isOpen
 * @param position
 */
const getAnimationToValue = (isOpen: boolean, position: Position) => {
  if (position === 'left') {
    return isOpen ? 0 : -MENU_WIDTH;
  } else {
    return isOpen ? SCREEN_WIDTH - MENU_WIDTH : SCREEN_WIDTH;
  }
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  },
  drawer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: '100%',
    width: MENU_WIDTH,
    backgroundColor: Theme.LightBackground,
    borderLeftColor: Theme.LightBorder,
    borderLeftWidth: 1,
  },
});
