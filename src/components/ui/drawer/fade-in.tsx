import React, { useEffect, useState } from 'react';
import { Animated, ViewProps } from 'react-native';

interface AnimProps extends ViewProps {
  trigger: boolean;
}

export const FadeIn: React.FC<AnimProps> = ({ style, trigger, ...props }) => {
  const [fadeAnim, setAnimValue] = useState(new Animated.Value(0));

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 150,
      useNativeDriver: true,
    }).start(() => setAnimValue(new Animated.Value(0)));
  }, [trigger]);

  return (
    <Animated.View
      style={{
        ...(style as Object),
        display: 'flex',
        opacity: fadeAnim.interpolate({
          inputRange: [0, 1],
          outputRange: trigger ? [0, 1] : [1, 0],
        }),
      }}
    >
      {props.children}
    </Animated.View>
  );
};
