import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { Colors } from '../../styles';

interface Props {
  text: string;
  type?: 'small';
}

export const Heading: React.FC<Props> = ({ text, type = 'small' }) => {
  const titleStyle = styles[type];

  return <Text style={titleStyle}>{text}</Text>;
};

const styles = StyleSheet.create({
  small: {
    fontWeight: 'bold',
    color: Colors.DarkGrey,
  },
});
