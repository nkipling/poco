import React from 'react';
import { StyleSheet, Text, View, ViewStyle } from 'react-native';
import { Colors } from '../../styles';

interface Props {
  color?: string;
  style?: ViewStyle;
}

export const Label: React.FC<Props> = ({ children, style = {} }) => {
  return (
    <View style={{ ...styles.container, ...style }}>
      <Text style={styles.text}>{children}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 16,
    backgroundColor: Colors.TableBorder,
    paddingVertical: 6,
    paddingHorizontal: 8,
  },
  text: {
    fontSize: 12,
  },
});
