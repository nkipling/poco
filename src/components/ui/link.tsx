import React from 'react';
import { Linking, Text, TextStyle } from 'react-native';

interface Props {
  url: string;
  children: any;
  style?: TextStyle;
}

export const Link: React.FC<Props> = ({ url, children, style }) => {
  // TODO: check for self hosted domain
  const isGitLabLink = url.includes('https://gitlab.com') && children[0].props.children === url;
  let linkText = children;

  if (isGitLabLink) {
    linkText = processGitLabLink(url);
  }

  const openUrl = () => {
    Linking.openURL(url);
  };

  return (
    <Text onPress={openUrl} style={style}>
      {linkText}
    </Text>
  );
};

const processGitLabLink = (url: string): string => {
  // This is a comment
  if (url.includes('note')) {
    const lastSlashIdx = url.lastIndexOf('/') + 1;
    const ids = url.substring(lastSlashIdx);
    const [issueId, commentId] = ids.split('#note_');

    return `#${issueId} (comment ${commentId})`;
  }

  return url;
};
