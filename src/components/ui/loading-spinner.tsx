import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React, { useEffect, useState } from 'react';
import { Animated, Easing, StyleSheet } from 'react-native';

interface Props {
  style?: any;
  size?: number;
  inline?: boolean;
}

export const LoadingSpinner: React.FC<Props> = ({ style = {}, size = 16, inline, ...props }) => {
  const [animValue] = useState(new Animated.Value(0));

  useEffect(() => {
    const animation = Animated.timing(animValue, {
      toValue: 1,
      duration: 2000,
      easing: Easing.linear,
      useNativeDriver: true,
    });

    Animated.loop(animation).start();
  }, [animValue]);

  useEffect(() => {});

  return (
    <Animated.View
      style={{
        ...styles.spinner,
        ...(inline ? styles.inline : {}),
        transform: [
          {
            rotate: animValue.interpolate({
              inputRange: [0, 1],
              outputRange: ['0deg', '360deg'],
            }),
          },
        ],
      }}
    >
      <FontAwesomeIcon icon="spinner" style={style} size={size} {...props} />
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  spinner: {
    borderRadius: 50,
    paddingVertical: 7.5,
    paddingHorizontal: 8.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inline: {
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
});
