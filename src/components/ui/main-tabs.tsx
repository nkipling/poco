import React, { ComponentType, useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { useSelector } from 'react-redux';
import { getMergeRequestsCount } from '../../store/selectors';
import { Brand, Colors } from '../../styles';
import { Issues, MergeRequests, Todos, User } from '../../views/tabbed-views';
import { TabButton, TabIcon } from './tab-button';

type Tab = {
  icon: TabIcon;
  title: string;
  component: ComponentType;
  badgeCount?: number;
  badgeColor?: string;
};

interface Props {
  onTabChange?: (title: string) => void;
}

export const MainTabsContainer: React.FC<Props> = props => {
  const [selectedTabIndex, setSelectedTab] = useState(1);
  const mergeRequestsCount = useSelector(getMergeRequestsCount);

  const tabs: Tab[] = [
    {
      icon: 'issues',
      title: 'Issues',
      component: Issues,
      badgeCount: 10,
      badgeColor: Brand.IssuesRequestBadgeColor,
    },
    {
      icon: 'merge-request',
      title: 'Merge Requests',
      component: MergeRequests,
      badgeCount: mergeRequestsCount,
      badgeColor: Brand.MergeRequestBadgeColor,
    },
    {
      icon: 'todo',
      title: 'Todos',
      component: Todos,
      badgeCount: 10,
      badgeColor: Brand.TodoRequestBadgeColor,
    },
    {
      icon: 'avy',
      title: 'TODO: Username',
      component: User,
    },
  ];

  useEffect(() => {
    if (props.onTabChange) {
      props.onTabChange(tabs[selectedTabIndex].title);
    }
  }, [selectedTabIndex]);

  const onTabButtonPress = (index: number) => {
    setSelectedTab(index);
  };

  const TabView = tabs[selectedTabIndex].component;

  return (
    <View style={styles.tabContainer}>
      <View style={styles.contentContainer}>
        <TabView />
      </View>
      <View style={styles.tabButtonsContainer}>
        {tabs.map((x, index) => (
          <TabButton
            icon={x.icon}
            onPress={() => onTabButtonPress(index)}
            selected={selectedTabIndex === index}
            key={index}
            badgeCount={x.badgeCount}
            badgeColor={x.badgeColor}
          />
        ))}
      </View>
    </View>
  );
};

const TAB_BAR_HEIGHT = 49;

const styles = StyleSheet.create({
  tabContainer: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
  },
  tabButtonsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: TAB_BAR_HEIGHT,
    borderTopWidth: 1,
    borderColor: Colors.BorderColor,
  },
});
