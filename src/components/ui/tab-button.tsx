import React, { useState } from 'react';
import {
  Animated,
  ImageURISource,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { useSelector } from 'react-redux';
import { Avatar } from '..';
import IssuesIcon from '../../assets/svgs/sprite_icons_issues.svg';
import MergeRequestIcon from '../../assets/svgs/sprite_icons_merge-request.svg';
import TodoIcon from '../../assets/svgs/sprite_icons_todo-done.svg';
import { User } from '../../models';
import { getCurrentUser } from '../../store/selectors';
import { Colors } from '../../styles';

export type TabIcon = 'avy' | 'issues' | 'merge-request' | 'todo';

interface Props {
  icon: TabIcon;
  selected?: boolean;
  badgeCount?: number;
  badgeColor?: string;
  onPress: () => void;
}

function getIcon(icon: TabIcon) {
  switch (icon) {
    case 'avy':
      return Avatar;

    case 'issues':
      return IssuesIcon;

    case 'merge-request':
      return MergeRequestIcon;

    case 'todo':
      return TodoIcon;
  }
}

export const TabButton: React.FC<Props> = props => {
  const user = useSelector(getCurrentUser) as User;
  const SvgIcon = getIcon(props.icon);
  let avy: ImageURISource | null = null;
  let showBadgeCount = Boolean(props.badgeCount && props.badgeCount > 0);
  let badgeCount: string = '';
  let badgeColor: string = props.badgeColor || '#1aaa55';
  const [animValue] = useState(new Animated.Value(1));

  if (user) {
    avy = { uri: user.avatar_url };
  }

  if (props.badgeCount) {
    if (props.badgeCount > 99) {
      badgeCount = '99+';
    } else {
      badgeCount = props.badgeCount.toString();
    }
  }

  const handlePressIn = () => {
    Animated.spring(animValue, {
      toValue: 0.8,
      useNativeDriver: true,
    }).start();
  };

  const handlePressOut = () => {
    Animated.spring(animValue, {
      toValue: 1,
      friction: 10,
      tension: 40,
      useNativeDriver: true,
    }).start();
  };

  const animatedStyle = {
    transform: [{ scale: animValue }],
  };

  /**
   * Apparently there are issues setting opacity directly on TouchableOpacity via styles.
   * Therefore, there is an extra <View> here so we can control the opacity of the buttons.
   * Too _fucking_ hard.
   */
  return (
    <TouchableWithoutFeedback
      onPress={props.onPress}
      onPressIn={handlePressIn}
      onPressOut={handlePressOut}
    >
      <Animated.View
        style={{ ...styles.buttonContainer, opacity: props.selected ? 1 : 0.5, ...animatedStyle }}
      >
        {showBadgeCount && (
          <View style={{ ...styles.badge, backgroundColor: badgeColor }}>
            <Text style={styles.badgeText}>{badgeCount}</Text>
          </View>
        )}
        <SvgIcon width={18} height={18} src={avy} fill={Colors.Black} size="sm" />
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    height: '100%',
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  badge: {
    position: 'absolute',
    right: 20,
    bottom: 15,
    paddingHorizontal: 5,
    borderRadius: 7,
    zIndex: 100,
    height: 14,
    alignItems: 'center',
    justifyContent: 'center',
  },
  badgeText: {
    fontWeight: '600',
    color: '#fff',
    fontSize: 12,
    lineHeight: 14,
  },
});
