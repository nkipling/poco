import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Avatar, ContentContainer } from '.';
import { User } from '../models';
import { Colors } from '../styles';

interface Props {
  user: User;
}

export const UserDetails: React.FC<Props> = ({ user, ...props }) => {
  return (
    <ContentContainer>
      <View style={styles.container}>
        <View style={styles.avatarContainer}>
          <Avatar src={{ uri: user.avatar_url }} size="xl" />
        </View>
        <View>
          <Text style={styles.name}>{user.name}</Text>

          <View style={styles.iconTextContainer}>
            <FontAwesomeIcon icon="at" style={{ ...styles.icon, ...styles.iconText }} />
            <Text style={styles.iconText}>{user.username}</Text>
          </View>

          <View style={styles.iconTextContainer}>
            <FontAwesomeIcon icon="toolbox" style={{ ...styles.icon, ...styles.iconText }} />
            <Text style={styles.iconText}>{user.organization}</Text>
          </View>
        </View>
      </View>
    </ContentContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: Colors.BorderColor,
  },
  avatarContainer: {
    marginRight: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  name: {
    fontSize: 26,
    marginBottom: 8,
  },
  iconTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 8,
  },
  icon: {
    marginRight: 4,
  },
  iconText: {
    fontSize: 16,
    lineHeight: 16,
    color: Colors.DarkGrey,
    alignSelf: 'center',
  },
});
