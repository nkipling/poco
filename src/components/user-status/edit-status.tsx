import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import * as emoji from 'node-emoji';
import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { LoadingSpinner } from '..';
import { UserStatusResponse } from '../../models';
import { setCurrentUserStatus } from '../../store/actions';
import { getUserIsLoading } from '../../store/selectors';
import { Brand, Colors } from '../../styles';

interface Props {
  status?: UserStatusResponse;
}

export const EditStatus: React.FC<Props> = ({ status }) => {
  const dispatch = useDispatch();
  const [editing, setEditing] = useState(false);
  const [validated, setValidated] = useState(false);
  const buttonStyle = validated ? styles.editSaveButtonValidated : styles.editSaveButtonDisabled;
  const iconStyle = validated
    ? styles.editSaveButtonIconValidated
    : styles.editSaveButtonIconDisabled;

  const loading = useSelector(getUserIsLoading);

  const onInput = (e: string) => {
    setValidated(e ? true : false);
  };

  const toggleEditing = () => {
    setEditing(!editing);
  };

  const saveStatus = () => {
    dispatch(setCurrentUserStatus(''));
  };

  return (
    <View style={styles.editFieldContainer}>
      <View style={{ ...styles.editEmojiField, ...(editing ? styles.editingEnabled : {}) }}>
        {status && status.emoji && <Text>{emoji.get(status.emoji)}</Text>}
      </View>
      <View style={{ ...styles.editTextField, ...(editing ? styles.editingEnabled : {}) }}>
        <TextInput
          onChangeText={onInput}
          onSubmitEditing={saveStatus}
          style={{ flex: 1, fontSize: 14, height: 22 }}
          editable={editing}
          defaultValue={(status && status.message) || ''}
        />
        {editing && (
          <TouchableOpacity style={styles.closeButton} onPress={toggleEditing}>
            <FontAwesomeIcon icon="times" style={{ color: Brand.PrimaryFont }} size={14} />
          </TouchableOpacity>
        )}
      </View>
      {loading ? (
        <LoadingSpinner />
      ) : editing ? (
        <TouchableOpacity style={{ ...styles.editSaveButton, ...buttonStyle }} onPress={saveStatus}>
          <FontAwesomeIcon icon="save" style={iconStyle} size={16} />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity style={{ ...styles.editButton }} onPress={toggleEditing}>
          <FontAwesomeIcon icon="pencil-alt" style={styles.editButtonIcon} size={16} />
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  editFieldContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  editEmojiField: {
    paddingVertical: 10.5,
    paddingHorizontal: 16,
    borderWidth: 1,
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    borderColor: 'transparent',
  },
  editTextField: {
    padding: 8,
    flex: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRightWidth: 1,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    borderColor: 'transparent',
    marginRight: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  closeButton: {
    alignSelf: 'flex-end',
    backgroundColor: Brand.Primary,
    borderRadius: 50,
    padding: 4,
  },
  editingEnabled: {
    borderColor: Colors.BorderColor,
  },
  editButton: {
    borderColor: Colors.BorderColor,
    borderWidth: 1,
    borderRadius: 50,
    padding: 7.5,
  },
  editButtonIcon: {
    color: Colors.DarkGrey,
  },
  editSaveButton: {
    borderWidth: 1,
    borderRadius: 50,
    padding: 7.5,
  },
  editSaveButtonIconValidated: {
    color: Colors.White,
  },
  editSaveButtonIconDisabled: {
    color: Colors.BorderColor,
  },
  editSaveButtonValidated: {
    borderColor: Brand.Success as string,
    backgroundColor: Brand.Success as string,
  },
  editSaveButtonDisabled: {
    borderColor: Colors.White as string,
    backgroundColor: Colors.White as string,
  },
});
