import * as emoji from 'node-emoji';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { UserStatusResponse } from '../../models';

interface Props {
  status: UserStatusResponse;
}

export const StatusDisplay: React.FC<Props> = ({ status }) => {
  return (
    <View style={styles.statusDisplayContainer}>
      {status && status.emoji && <Text style={styles.emojiText}>{emoji.get(status.emoji)}</Text>}
      {status && <Text>{status.message}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  statusDisplayContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  emojiText: {
    fontSize: 20,
    marginRight: 4,
  },
});
