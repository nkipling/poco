import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React, { useEffect, useState } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ContentContainer, Heading } from '../';
import { UserStatusResponse } from '../../models';
import { fetchCurrentUserStatus } from '../../store/actions';
import { getCurrentUserStatus } from '../../store/selectors';
import { Colors } from '../../styles';
import { EditStatus } from './edit-status';
import { StatusDisplay } from './status-display';

export const UserStatus: React.FC = () => {
  const dispatch = useDispatch();
  const userStatus = useSelector(getCurrentUserStatus) as UserStatusResponse;
  const [editing, setEditing] = useState(true);

  useEffect(() => {
    dispatch(fetchCurrentUserStatus());
  }, [dispatch]);

  const editStatus = () => {
    setEditing(true);
  };

  return (
    <ContentContainer>
      <View style={styles.container}>
        <Heading text="Current Status" type="small" />

        {editing && (
          <View style={styles.statusContainer}>
            <EditStatus status={userStatus} />
          </View>
        )}

        {!editing && (
          <View style={styles.statusContainer}>
            <StatusDisplay status={userStatus} />
            <TouchableOpacity onPress={editStatus}>
              <FontAwesomeIcon icon="pencil-alt" style={styles.editButton} size={18} />
            </TouchableOpacity>
          </View>
        )}
      </View>
    </ContentContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: Colors.BorderColor,
  },
  statusContainer: {
    flexDirection: 'row',
    paddingTop: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  editButton: {
    color: Colors.DarkGrey,
  },
});
