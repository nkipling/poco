import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faAt,
  faBars,
  faChevronLeft,
  faCog,
  faLock,
  faPencilAlt,
  faSave,
  faSpinner,
  faTimes,
  faToolbox,
  faUnlock,
} from '@fortawesome/free-solid-svg-icons';

export const initIcons = () =>
  library.add(
    faAngleDoubleLeft,
    faAngleDoubleRight,
    faAt,
    faBars,
    faChevronLeft,
    faCog,
    faLock,
    faPencilAlt,
    faSave,
    faSpinner,
    faTimes,
    faToolbox,
    faUnlock,
  );
