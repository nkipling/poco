import React, { useMemo } from 'react';
import { View } from 'react-native';
import { parseMarkdown } from './/parser';

interface Props {
  styles?: any;
}

export const Markdown: React.FC<Props> = ({ children, styles = {} }) => {
  const res = useMemo(() => parseMarkdown(children as string, styles), [children]);

  return <View style={{ paddingBottom: 16 }}>{res}</View>;
};
