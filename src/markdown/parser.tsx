import { ReactNode } from 'react';
import markdown from 'remark-parse';
import unified from 'unified';
import { Node } from 'unist';
import { renderers, RenderFunction } from './render';

export const parseMarkdown = (str: string, customStyleRules: any) => {
  const { children = [] } = unified()
    .use(markdown, { commonmark: true })
    .parse(str);

  console.log('Children:', children);

  return (children as Node[]).map(x => renderNode(x, customStyleRules));
};

/**
 * Recursive function that renders and traverses down the child nodes from the
 * initial node it is passed.
 * @param node Initial node to render
 * @param customStyles An optional Stylesheet object to override the default styles
 * @param parent An optional parent that is passed to the render function
 */
const renderNode = (node: Node, customStyles: any = {}, parent?: Node) => {
  const renderFunction: RenderFunction = renderers[node.type];

  if (renderFunction) {
    const { children: nodeChildren = [] } = node;
    const children: ReactNode[] = (nodeChildren as Node[]).map(x =>
      renderNode(x, customStyles[x.type], node),
    );

    return renderFunction(node, children, parent, customStyles[node.type]);
  }

  console.warn('Unknown markdown node type:', node.type, node);

  // Note: nothing is rendered for unknown node types
  return null;
};
