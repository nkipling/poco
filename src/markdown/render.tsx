import React, { ReactNode } from 'react';
import { StyleProp, Text, TextStyle, View } from 'react-native';
import FitImage from 'react-native-fit-image';
import { Node } from 'unist';
import * as uuid from 'uuid';
import { Link } from '../components';
import { styles } from './styles';

export type RenderFunction = (
  node: Node,
  children?: ReactNode[],
  parent?: Node,
  styles?: any,
) => ReactNode;

type Renderders = {
  [key: string]: RenderFunction;
};

const generateKey = (): string => {
  return uuid.v4();
};

export const renderers: Renderders = {
  heading: (node: Node, children?: ReactNode[]) => {
    // F****** love typescript
    const { type, depth } = node;
    const styleProp = `${type}${depth}` as keyof typeof styles;
    const headingStyle = [styles.heading, (styles[styleProp] as StyleProp<TextStyle>) || {}];

    return (
      <Text key={generateKey()} style={headingStyle}>
        {children}
      </Text>
    );
  },

  html: (node: Node, children?: ReactNode[]) => {
    if ((node.value as string).includes('<!--')) {
      return null;
    } else {
      console.warn('HTML elments found during markdown:', node);

      // TODO: Handle these somehow

      return null;
    }
  },

  image: (node: any) => {
    let uri;

    if (node.url.includes('http')) {
      uri = node.url;
    } else {
      // TODO: Change this domain to the base for the current GitLab instance.
      // That project path tho... (maybe from references?)
      uri = `https://gitlab.com/gitlab-org/gitlab${node.url}`;
    }

    return (
      <FitImage
        indicator={true}
        source={{ uri }}
        key={generateKey()}
        style={{ flex: 1, ...styles.image }}
      />
    );
  },

  inlineCode: (node: Node) => (
    <Text key={generateKey()} style={styles.inlineCode}>
      {node.value as string}
    </Text>
  ),

  link: (node: Node, children?: ReactNode[]) => (
    <Link key={generateKey()} url={node.url as string} style={styles.link}>
      {children}
    </Link>
  ),

  list: (node: Node, children?: ReactNode[]) => {
    return <View key={generateKey()}>{children}</View>;
  },

  listItem: (node: Node, children?: ReactNode[], parent?: Node) => {
    return (
      <View key={generateKey()} style={styles.listUnorderedItem}>
        {parent?.ordered ? (
          <Text style={styles.listUnorderedItemIcon}>1.</Text>
        ) : (
          <Text style={styles.listUnorderedItemIcon}>{'\u00B7'}</Text>
        )}
        <Text style={styles.listItem}>{children}</Text>
      </View>
    );
  },

  paragraph: (node: Node, children?: ReactNode[], parent?: Node, customStyles?: any) => (
    <Text key={generateKey()} style={[styles.paragraph, customStyles]}>
      {children}
    </Text>
  ),

  strong: (node: Node, children?: ReactNode[]) => (
    <Text key={generateKey()} style={styles.strong}>
      {children}
    </Text>
  ),

  table: (node: Node, children?: ReactNode[]) => (
    <View key={generateKey()} style={styles.table}>
      {children}
    </View>
  ),

  tableRow: (node: Node, children?: ReactNode[]) => (
    <View key={generateKey()} style={styles.tableRow}>
      {children}
    </View>
  ),

  tableCell: (node: Node, children?: ReactNode[]) => (
    <View key={generateKey()} style={styles.tableCell}>
      {children}
    </View>
  ),

  text: (node: Node, children?: ReactNode[]) => (
    <Text key={generateKey()} style={styles.text}>
      {node.value as string}
    </Text>
  ),
};
