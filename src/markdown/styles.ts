import { Platform, StyleSheet } from 'react-native';
import { Brand, Colors } from '../styles';

export const styles = StyleSheet.create({
  heading: {
    fontWeight: 'bold',
  },
  heading2: {
    fontSize: 21,
    marginTop: 24,
  },
  heading3: {
    fontSize: 18,
    marginTop: 24,
  },
  heading4: {
    fontSize: 16,
    marginTop: 20,
  },
  heading5: {
    fontSize: 13,
    marginTop: 17,
  },
  heading6: {
    fontSize: 11,
    marginTop: 15,
  },
  image: {
    borderWidth: 1,
    borderColor: Colors.BorderColor,
  },
  inlineCode: {
    borderRadius: 4,
    paddingHorizontal: 3,
    backgroundColor: '#fbe5e1',
    color: '#c0341d',
  },
  link: {
    color: Brand.Link,
  },
  listItem: {
    flex: 1,
    flexWrap: 'wrap',
  },
  listUnorderedItem: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    lineHeight: 20,
    marginTop: 10,
  },
  listUnorderedItemIcon: {
    marginLeft: 10,
    marginRight: 10,
    fontSize: 30,
    ...Platform.select({
      ['ios']: {
        lineHeight: 25,
      },
      ['android']: {
        lineHeight: 19,
      },
    }),
  },
  paragraph: {
    marginTop: 10,
    marginBottom: 10,
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  strong: {
    fontWeight: 'bold',
  },
  table: {
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderRadius: 4,
    borderColor: Colors.TableBorder,
    marginTop: 10,
  },
  tableRow: {
    borderColor: Colors.TableBorder,
    borderBottomWidth: 1,
    flexDirection: 'row',
  },
  tableCell: {
    flex: 1,
    padding: 5,
  },
  text: {
    lineHeight: 20,
  },
});
