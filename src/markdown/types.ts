import { Token } from 'markdown-it';

export interface MarkdownElement {
  attrs: any;
  block: boolean;
  content: string;
  children: MarkdownElement[];
  rawChildren: Token[];
  elements: any[];
  hidden: boolean;
  markup: string;
  tag: string;
  type: string;
}
