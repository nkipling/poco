import { Token } from 'markdown-it';
import { MarkdownElement } from './types';

/**
 * Small util function to convert a Token to MarkdownElement
 * @param token
 */
export const convertToMarkdownElement = (token: Token): MarkdownElement => {
  const { attrs, block, content, hidden, markup, tag, type } = token;

  return {
    attrs,
    block,
    content,
    children: [],
    rawChildren: [],
    elements: [],
    hidden,
    markup,
    tag,
    type: type.replace('_open', ''),
  };
};
