export interface MergeRequest {
  id: number;
  iid: number;
  project_id: number;
  title: string;
  description: string;
  state: string;
  merged_by?: MergeRequestUser;
  merged_at: string;
  closed_by?: MergeRequestUser;
  closed_at: string;
  created_at: string;
  updated_at: string;
  target_branch: string;
  source_branch: string;
  upvotes: number;
  downvotes: number;
  author?: MergeRequestUser;
  assignee?: MergeRequestUser;
  assignees: MergeRequestUser[];
  source_project_id: number;
  target_project_id: number;
  labels: string[];
  work_in_progress: boolean;
  milestone: {
    id: number;
    iid: number;
    project_id: number;
    title: string;
    description: string;
    state: string;
    created_at: string;
    updated_at: string;
    due_date: string;
    start_date: string;
    web_url: string;
  };
  merge_when_pipeline_succeeds: boolean;
  merge_status: string;
  sha: string;
  has_conflicts: boolean;
  merge_commit_sha: string;
  squash_commit_sha: string;
  user_notes_count: number;
  discussion_locked: boolean;
  should_remove_source_branch: boolean;
  force_remove_source_branch: boolean;
  allow_collaboration: boolean;
  allow_maintainer_to_push: boolean;
  web_url: string;
  references: {
    short: string;
    relative: string;
    full: string;
  };
  time_stats: {
    time_estimate: number;
    total_time_spent: number;
    human_time_estimate: number;
    human_total_time_spent: number;
  };
  squash: boolean;
  task_completion_status: {
    count: number;
    completed_count: number;
  };
  fetchingPipeline?: boolean;
  pipeline?: MergeRequestPipeline | boolean;
  head_pipeline?: MergeRequestHeadPipeline;
  notes?: MergeRequestNote[];
}

export interface MergeRequestUser {
  id: number;
  name: string;
  username: string;
  state: string;
  avatar_url: string;
  web_url: string;
}

export interface MergeRequestPipeline {
  id: number;
  sha: string;
  ref: string;
  status: string;
  web_url: string;
}

export interface MergeRequestHeadPipeline extends MergeRequestPipeline {
  before_sha: string;
  tag: boolean;
  user: MergeRequestUser;
  started_at: string;
  finished_at: string;
  committed_at: string;
  duration: number;
  coverage: string;
  detailed_status: {
    icon: string;
    text: string;
    label: string;
    group: string;
    tooltip: string;
    has_details: boolean;
    details_path: string;
    illustration: string;
    favion: string;
  };
}

export interface MergeRequestNote {
  attachment: string | null;
  author: MergeRequestUser;
  body: string;
  created_at: string;
  id: number;
  noteable_id: number;
  notetable_iid: number;
  noteable_type: 'MergeRequest'; // Probably more types...
  resolvable: boolean;
  system: boolean;
  type: string; // Not sure what the options are here
  updated_at: string;
}
