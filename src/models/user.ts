export interface User {
  avatar_url: string;
  bio: string;
  can_create_group: boolean;
  can_create_project: boolean;
  color_scheme_id: number;
  confirmed_at: string;
  created_at: string;
  current_sign_in_at: string;
  email: string;
  external: boolean;
  extra_shared_runners_minutes_limit: number;
  id: number;
  identities: Object[];
  last_activity_on: string;
  last_sign_in_at: string;
  linkedin: string;
  location: string;
  name: string;
  organization: string;
  private_profile: boolean;
  projects_limit: number;
  public_email: string;
  shared_runners_minutes_limit: number;
  skype: string;
  state: string;
  theme_id: number;
  twitter: string;
  two_factor_enabled: boolean;
  username: string;
  web_url: string;
  website_url: string;
}

export interface UserStatusResponse {
  emoji: string;
  message: string;
  message_html: string;
}
