import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import axios from '../../axios';
import { MergeRequest } from '../../models';
import { AppState } from '../reducers';

type FetchingDetail = {
  id: number;
  fetching: boolean;
};

// === SYNC ACTIONS === //

const SET_MERGE_REQUESTS = 'SET_MERGE_REQUESTS';
const UPDATE_MERGE_REQUEST_DETAIL = 'UPDATE_MERGE_REQUEST_DETAIL';
const SET_SELECTED_MERGE_REQUEST = 'SET_SELECTED_MERGE_REQUEST';
const SET_IS_LOADING = 'SET_IS_LOADING';
const RESET_MERGE_REQUEST_STATE = 'RESET_MERGE_REQUEST_STATE';
const SET_IS_FETCHING_NOTES = 'SET_IS_FETCHING_NOTES';

const setMergeRequests = createAction(SET_MERGE_REQUESTS)<MergeRequest[]>();
const updateMergeRequest = createAction(UPDATE_MERGE_REQUEST_DETAIL)<MergeRequest>();
const setSelectedMergeRequest = createAction(SET_SELECTED_MERGE_REQUEST)<number | null>();
const setIsLoading = createAction(SET_IS_LOADING)<boolean>();
const resetMergeRequestsState = createAction(RESET_MERGE_REQUEST_STATE)<void>();
const setIsFetchingNotes = createAction(SET_IS_FETCHING_NOTES)<boolean>();

export const mergeRequestActions = {
  setMergeRequests,
  updateMergeRequest,
  setSelectedMergeRequest,
  setIsLoading,
  resetMergeRequestsState,
  setIsFetchingNotes,
};

export type MergeRequestActions = ActionType<typeof mergeRequestActions>;

// === ASYNC ACTIONS === //

export const fetchMergeRequests = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    dispatch(setIsLoading(true));

    try {
      const { data } = await axios.get(
        '/merge_requests?state=opened&order_by=updated_at&scope=assigned_to_me',
      );
      dispatch(setMergeRequests(data));
    } catch (error) {
      // TODO: something...
      console.log(error);
    } finally {
      dispatch(setIsLoading(false));
    }
  };
};

export const fetchMergeRequestDetail = (projectId: number, mergeRequestId: number) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    const state: AppState = getState();
    const mr = state.mergeRequests.list.find(x => x.iid === mergeRequestId);

    if (mr) {
      try {
        const { data } = await axios.get(`/projects/${projectId}/merge_requests/${mergeRequestId}`);
        dispatch(updateMergeRequest(data));
      } catch (ex) {
        mr.pipeline = true;
        dispatch(updateMergeRequest(mr));
      }
    }
  };
};

export const fetchMergeRequestNotes = (projectId: number, mergeRequestId: number) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    // dispatch(setIsFetchingNotes(true));
    const state: AppState = getState();
    const mr = state.mergeRequests.list.find(x => x.iid === mergeRequestId);

    if (mr) {
      try {
        const { data } = await axios.get(
          `/projects/${projectId}/merge_requests/${mergeRequestId}/notes?sort=asc`,
        );

        mr.notes = data;
        dispatch(updateMergeRequest(mr));
      } catch (ex) {
        // TODO: handle fail
      } finally {
      }
    }
  };
};
