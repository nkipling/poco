import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import axios from '../../axios';
import { AppState } from '../reducers';

// === SYNC ACTIONS === //

const SET_TODO_LIST = 'SET_TODO_LIST';
const SET_IS_LOADING = 'SET_IS_LOADING';

export const setTodoList = createAction(SET_TODO_LIST)<string[]>();
export const setIsLoading = createAction(SET_IS_LOADING)<boolean>();

export const todoActions = {
  setTodoList,
  setIsLoading,
};

export type TodoActions = ActionType<typeof todoActions>;

// === ASYNC ACTIONS === //

export const getTodos = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    try {
      const { data } = await axios.get('/todos');

      console.log(data);

      dispatch(setTodoList(data));
    } catch (error) {
      // TODO: something...
      console.log(error);
    }
  };
};
