import { authorize, AuthorizeResult } from 'react-native-app-auth';
import { OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET } from 'react-native-dotenv';
import { ThunkDispatch } from 'redux-thunk';
import { Action, ActionType, createAction } from 'typesafe-actions';
import { fetchMergeRequests, mergeRequestActions } from '.';
import axios from '../../axios';
import history from '../../history';
import { User, UserStatusResponse } from '../../models';
import { AppState } from '../reducers';

const authConfig = {
  issuer: 'https://gitlab.com',
  clientId: OAUTH_CLIENT_ID,
  clientSecret: OAUTH_CLIENT_SECRET,
  redirectUrl: 'gitlabnative://oauth-redirect',
  scopes: ['api', 'read_user', 'read_repository', 'write_repository', 'read_registry'],
};

// === SYNC ACTIONS === //

const SET_AUTH_STATE = 'SET_AUTH_STATE';
const SET_USER = 'SET_USER';
const SET_USER_STATUS = 'SET_USER_STATUS';
const SET_LOGGED_IN = 'SET_LOGGED_IN';
const SET_IS_LOADING = 'SET_IS_LOADING';
const RESET_USER_STATE = 'RESET_USER_STATE';

export const setAuthState = createAction(SET_AUTH_STATE)<AuthorizeResult>();
export const setUser = createAction(SET_USER)<User | null>();
export const setUserStatus = createAction(SET_USER_STATUS)<UserStatusResponse>();
export const setLoggedIn = createAction(SET_LOGGED_IN)<boolean>();
export const setIsLoading = createAction(SET_IS_LOADING)<boolean>();
const resetUserState = createAction(RESET_USER_STATE)<void>();

export const userActions = {
  setAuthState,
  setUser,
  setUserStatus,
  setLoggedIn,
  setIsLoading,
  resetUserState,
};

export type UserActions = ActionType<typeof userActions>;

// === ASYNC ACTIONS === //

export const startUserSignIn = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    try {
      const result = await authorize(authConfig);
      console.log(result);

      dispatch(setAuthState(result));

      // Set auth token for API requests
      axios.defaults.headers.common['Authorization'] = `Bearer ${result.accessToken}`;

      dispatch(getCurrentUserDetails());
      dispatch(fetchMergeRequests());

      // Navigate to the main tabs screen
      history.push('/main-tabs');
    } catch (error) {
      // TODO: something...
      console.log(error);
    }
  };
};

export const signOutUser = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>, getState: Function) => {
    const state: AppState = getState();

    try {
      // This is a fake logout, I don't think we can actually revoke an auth token via the API.
      // So clear all local state and hope no one tries to log back in for a while...
      // TODO: Fix this at some point

      dispatch(mergeRequestActions.resetMergeRequestsState());
      dispatch(resetUserState());

      history.push('/');
    } catch (ex) {
      // TODO: handle errors
      console.log(ex);
    }
  };
};

export const getCurrentUserDetails = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    try {
      const { data } = await axios.get('/user');
      dispatch(setUser(data));
      dispatch(setLoggedIn(true));

      console.log('User:', data);
    } catch (error) {
      // TODO: something...
      console.log(error);
    }
  };
};

export const fetchCurrentUserStatus = () => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    try {
      const { data } = await axios.get('/user/status');

      dispatch(setUserStatus(data));
    } catch (ex) {
      console.log(ex);
      // Do nothing - we don't care if this fails
    }
  };
};

export const setCurrentUserStatus = (status: string) => {
  return async (dispatch: ThunkDispatch<AppState, void, Action>) => {
    dispatch(setIsLoading(true));

    try {
      // TODO: add api logic

      setTimeout(() => dispatch(setIsLoading(false)), 1500);
    } catch (ex) {
      // TODO: handle error
      console.log(ex);
    }
  };
};
