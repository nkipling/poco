import { combineReducers } from 'redux';
import mergeRequests, { MergeRequestState } from './merge-requests';
import todos, { TodoState } from './todos';
import user, { UserState } from './user';

export interface AppState {
  user: UserState;
  todos: TodoState;
  mergeRequests: MergeRequestState;
}

export default combineReducers({ user, todos, mergeRequests });
