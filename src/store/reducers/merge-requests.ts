import { createReducer } from 'typesafe-actions';
import { MergeRequest } from '../../models';
import { MergeRequestActions, mergeRequestActions } from '../actions';

export interface MergeRequestState {
  list: MergeRequest[];
  isLoading: boolean;
  selectedMergeRequest: number | null;
  isFetchingNotes: boolean;
}

const initialState: MergeRequestState = {
  list: [],
  isLoading: false,
  selectedMergeRequest: null,
  isFetchingNotes: false,
};

export default createReducer<MergeRequestState, MergeRequestActions>(initialState)
  .handleAction(mergeRequestActions.setMergeRequests, (state, action) => ({
    ...state,
    list: action.payload || [],
  }))
  .handleAction(mergeRequestActions.updateMergeRequest, (state, action) => {
    const existingMRIndex = state.list.findIndex(x => x.iid === action.payload.iid);

    // TODO: tidy this...

    if (existingMRIndex > -1) {
      const mrWithDetail = {
        ...state.list[existingMRIndex],
        ...action.payload,
      };

      const newMRList = state.list;
      newMRList[existingMRIndex] = mrWithDetail;

      return {
        ...state,
        list: [...newMRList],
      };
    }

    return state;
  })
  .handleAction(mergeRequestActions.setSelectedMergeRequest, (state, action) => ({
    ...state,
    selectedMergeRequest: action.payload,
  }))
  .handleAction(mergeRequestActions.setIsLoading, (state, action) => ({
    ...state,
    isLoading: action.payload || false,
  }))
  .handleAction(mergeRequestActions.setIsFetchingNotes, (state, action) => ({
    ...state,
    isFetchingNotes: action.payload || false,
  }))
  .handleAction(mergeRequestActions.resetMergeRequestsState, () => initialState);
