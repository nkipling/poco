import { createReducer } from 'typesafe-actions';
import { todoActions, TodoActions } from '../actions/todos';

export interface TodoState {
  list: string[];
  isLoading: boolean;
}

const initialState: TodoState = {
  list: [],
  isLoading: false,
};

export default createReducer<TodoState, TodoActions>(initialState)
  .handleAction(todoActions.setTodoList, (state, action) => ({
    ...state,
    list: action.payload || [],
  }))
  .handleAction(todoActions.setIsLoading, (state, action) => ({
    ...state,
    isLoading: action.payload || false,
  }));
