import { AuthorizeResult } from 'react-native-app-auth';
import { createReducer } from 'typesafe-actions';
import { User, UserStatusResponse } from '../../models';
import { UserActions, userActions } from '../actions';

export interface UserState {
  user: User | null;
  userStatus: UserStatusResponse | null;
  authState: AuthorizeResult | null;
  loggedIn: boolean;
  isLoading: boolean;
}

const initialState: UserState = {
  user: null,
  userStatus: null,
  authState: null,
  loggedIn: false,
  isLoading: false,
};

export default createReducer<UserState, UserActions>(initialState)
  .handleAction(userActions.setAuthState, (state, action) => ({
    ...state,
    authState: action.payload || null,
  }))
  .handleAction(userActions.setUser, (state, action) => ({
    ...state,
    user: action.payload || null,
  }))
  .handleAction(userActions.setUserStatus, (state, action) => {
    let userStatus: UserStatusResponse | null = null;

    if (action.payload.message) {
      userStatus = action.payload;
    }

    return {
      ...state,
      userStatus,
    };
  })
  .handleAction(userActions.setLoggedIn, (state, action) => ({
    ...state,
    loggedIn: action.payload,
  }))
  .handleAction(userActions.setIsLoading, (state, action) => ({
    ...state,
    isLoading: action.payload,
  }))
  .handleAction(userActions.resetUserState, () => initialState);
