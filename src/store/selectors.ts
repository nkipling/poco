import { AppState } from './reducers';

// === User === //

export const getAuthState = (store: AppState) => {
  return store.user.authState || null;
};

export const getLoggedIn = (store: AppState) => {
  return store.user.loggedIn || false;
};

export const getUserIsLoading = (store: AppState) => {
  return store.user.isLoading || false;
};

export const getCurrentUser = (store: AppState) => {
  return store.user.user;
};

export const getCurrentUserStatus = (store: AppState) => {
  return store.user.userStatus;
};

// === Merge Requests === //

export const getMrLoading = (store: AppState) => {
  return store.mergeRequests.isLoading;
};

export const getMergeRequests = (store: AppState) => {
  return store.mergeRequests.list;
};

export const getDashboardMergeRequests = (store: AppState) => {
  return store.mergeRequests.list.slice(0, 3);
};

export const getMergeRequestsCount = (store: AppState) => {
  return store.mergeRequests.list.length;
};

export const getMergeRequestById = (store: AppState, mergeRequestId: number) => {
  const mr = store.mergeRequests.list.find(x => x.iid === mergeRequestId);

  return mr || null;
};

export const getSelectedMergeRequest = (store: AppState) => {
  const { selectedMergeRequest } = store.mergeRequests;

  if (selectedMergeRequest) {
    return store.mergeRequests.list.find(x => x.iid === selectedMergeRequest);
  }

  return null;
};
