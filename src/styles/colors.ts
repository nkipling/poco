export enum Brand {
  Orange = '#fc6d26',
  PurpleLight = '#6e49cb',
  Primary = '#007bff',
  PrimaryFont = '#fff',
  Success = '#27a745',
  SuccessFont = '#fff',
  Fail = '#db3b20',
  FailFont = '#fff',
  Info = '1e78d1',
  InfoFont = '#fff',

  IssuesRequestBadgeColor = '#1baa55',
  MergeRequestBadgeColor = '#de7e01',
  TodoRequestBadgeColor = '#1f78d1',

  Link = '#1b69b6',
}

export enum Colors {
  Black = '#000000',
  White = '#ffffff',
  DarkGrey = '#707070',
  BorderColor = '#e5e5e5',
  PipelineCancelled = '#2d2d2d',
  PipelineFailed = '#db3b21',
  PipelineRunning = '#1e78d1',
  PipelineSkipped = '#a7a7a7',
  PipelineSuccess = '#1baa55',
  PipelineWarning = '#fc9403',
  TableBorder = '#dfdfdf',
}

// TODO: Create some way of customising and setting the "theme"
export const Theme = {
  LightBackground: '#fafafa',
  LightBorder: '#e5e5e5',
  SecondaryAction: '#707070',

  Heading1: '#000',
  Heading2: '#2e2e2e',
};
