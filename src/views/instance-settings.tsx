import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { HeaderBar, StatusBarBg } from '../components';
import { Brand } from '../styles';

export const InstanceSettings: React.FC = () => {
  return (
    <>
      <StatusBarBg barStyle="light-content" backgroundColor={Brand.PurpleLight} />
      <SafeAreaView style={styles.flexGrow}>
        <HeaderBar title="Instance Settings" showBack />

        <View>
          <Text>Instance settings coming soon</Text>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  flexGrow: {
    flexGrow: 1,
  },
});
