import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Text } from 'react-native';
import { HeaderBar, MainTabsContainer, StatusBarBg } from '../components';
import { Brand } from '../styles';

export const MainTabsView: React.FC = () => {
  const [title, setTitle] = useState('');

  const onTabChange = (title: string) => {
    setTitle(title);
  };

  return (
    <>
      <StatusBarBg barStyle="light-content" backgroundColor={Brand.PurpleLight} />
      <SafeAreaView style={styles.flexGrow}>
        <HeaderBar showMenu title={title} menuFunction={() => {}} />

        <MainTabsContainer onTabChange={onTabChange}>
          <Text>Merge requests</Text>
        </MainTabsContainer>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  flexGrow: {
    flexGrow: 1,
  },
});
