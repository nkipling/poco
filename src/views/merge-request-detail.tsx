import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  NativeScrollEvent,
  NativeSyntheticEvent,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import MergeRequestIcon from '../assets/svgs/sprite_icons_merge-request.svg';
import ErrorIcon from '../assets/svgs/status_warning.svg';
import { Avatar, Drawer, HeaderBar, StatusBarBg } from '../components';
import { InnerDrawer } from '../components/merge-requests/drawer-menu';
import { MergeRequestNotes } from '../components/merge-requests/notes';
import { PipelineStatusDescription } from '../components/merge-requests/pipeline-status-description';
import { PipelineStatusIcon } from '../components/merge-requests/pipeline-status-icon';
import { StatusDisplay } from '../components/merge-requests/status-display';
import { Markdown } from '../markdown/markdown';
import { MergeRequest, MergeRequestPipeline } from '../models';
import { fetchMergeRequestNotes } from '../store/actions';
import { getSelectedMergeRequest } from '../store/selectors';
import { Brand, Colors } from '../styles';

export const MergeRequestDetail: React.FC = () => {
  const dispatch = useDispatch();
  const mergeRequest = useSelector(getSelectedMergeRequest);
  const [menuOpen, setMenuOpen] = useState(false);
  const menuIcon = menuOpen ? 'angle-double-right' : 'angle-double-left';
  const [showLoader, setShowLoader] = useState(false);

  useEffect(() => {
    if (showLoader) {
      console.log('Merged request updated:', mergeRequest);
      setShowLoader(false);
    }
  }, [mergeRequest]);

  if (!mergeRequest) {
    return null;
  }

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  const onScroll = (e: NativeSyntheticEvent<NativeScrollEvent>) => {
    const { layoutMeasurement, contentOffset, contentSize } = e.nativeEvent;
    const bottomPadding = 20;

    if (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - bottomPadding &&
      !showLoader
    ) {
      setShowLoader(true);
      dispatch(fetchMergeRequestNotes(mergeRequest.project_id, mergeRequest.iid));
    }
  };

  return (
    <>
      <StatusBarBg barStyle="light-content" backgroundColor={Brand.PurpleLight} />
      <SafeAreaView style={styles.flexGrow}>
        <HeaderBar
          title={mergeRequest.references.short}
          showBack
          rightIcon={menuIcon}
          rightAction={toggleMenu}
        />

        <ScrollView style={styles.innerView} onScroll={onScroll} scrollEventThrottle={300}>
          <View style={{ ...styles.statusView, paddingTop: 8 }}>
            <View style={{ flex: 1 }}>
              <Text style={{ marginBottom: 4 }}>
                Opened {moment(mergeRequest.created_at).fromNow()} by
              </Text>
              <View style={styles.author}>
                <Avatar
                  src={{ uri: mergeRequest.author?.avatar_url }}
                  size="sm"
                  style={styles.authorAvatar}
                />
                <Text style={styles.authorText}>{mergeRequest.author?.name}</Text>
              </View>
            </View>

            <StatusDisplay status={mergeRequest.state} style={{ marginLeft: 16 }} />
          </View>

          {statusView(mergeRequest)}

          <View style={styles.statusView}>
            <PipelineStatusIcon
              pipeline={
                mergeRequest?.head_pipeline || (mergeRequest?.pipeline as MergeRequestPipeline)
              }
            />
            <View style={{ marginLeft: 8, flex: 1 }}>
              <PipelineStatusDescription
                pipeline={
                  mergeRequest?.head_pipeline || (mergeRequest?.pipeline as MergeRequestPipeline)
                }
              />
            </View>
          </View>

          <Text style={styles.title}>{mergeRequest.title}</Text>

          <Markdown>{mergeRequest.description}</Markdown>

          <View
            style={{ ...styles.statusView, borderTopWidth: 1, paddingTop: 16, marginBottom: 0 }}
          >
            <MergeRequestIcon width={18} height={18} fill={Colors.DarkGrey} />
            <View style={{ marginLeft: 8, flex: 1 }}>
              <Text>
                Request to merge {mergeRequest.source_branch} into {mergeRequest.target_branch}
              </Text>
            </View>
          </View>

          <MergeRequestNotes mergeRequest={mergeRequest} />

          {/* {showLoader && (
            <View style={[styles.statusView, { justifyContent: 'center', borderBottomWidth: 0 }]}>
              <LoadingSpinner />
            </View>
          )} */}
        </ScrollView>

        <Drawer open={menuOpen} position="right" onClose={() => setMenuOpen(false)}>
          <InnerDrawer mergeRequest={mergeRequest} closePress={() => setMenuOpen(false)} />
        </Drawer>
      </SafeAreaView>
    </>
  );
};

const statusView = (mergeRequest: MergeRequest) => {
  if (mergeRequest.has_conflicts) {
    return (
      <View style={styles.statusView}>
        <ErrorIcon width={22} height={22} fill={Colors.PipelineWarning} />
        <View style={{ marginLeft: 8, flex: 1 }}>
          <Text>Cannot be merged, there are merge conflicts</Text>
        </View>
      </View>
    );
  }

  if (mergeRequest.work_in_progress) {
    return (
      <View style={styles.statusView}>
        <ErrorIcon width={22} height={22} fill={Colors.DarkGrey} />
        <View style={{ marginLeft: 8, flex: 1 }}>
          <Text>Cannot be merged, this is a Work in Progress</Text>
        </View>
      </View>
    );
  }

  return null;
};

const styles = StyleSheet.create({
  flexGrow: {
    flexGrow: 1,
  },
  innerView: {
    flex: 1,
    padding: 10,
  },
  statusView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
    borderBottomWidth: 1,
    borderColor: Colors.BorderColor,
    paddingBottom: 16,
  },
  author: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  authorAvatar: {
    marginRight: 4,
  },
  authorText: {
    fontWeight: 'bold',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  loadingContainer: {
    paddingVertical: 16,
  },
});
