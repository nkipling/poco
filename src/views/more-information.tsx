import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { HeaderBar, StatusBarBg } from '../components';
import { Brand } from '../styles';

export const MoreInformation: React.FC = () => {
  return (
    <>
      <StatusBarBg barStyle="light-content" backgroundColor={Brand.PurpleLight} />
      <SafeAreaView style={styles.flexGrow}>
        <HeaderBar title="More Information" showBack />

        <View>
          <Text>More information</Text>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  flexGrow: {
    flexGrow: 1,
  },
});
