import React from 'react';
import { FlatList, RefreshControl, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-native';
import { MergeRequestSummary } from '../../components';
import { fetchMergeRequests, mergeRequestActions } from '../../store/actions';
import { getMergeRequests, getMrLoading } from '../../store/selectors';

export const MergeRequests: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const mergeRequests = useSelector(getMergeRequests);
  const isRefreshing = useSelector(getMrLoading);

  const onRefresh = () => {
    dispatch(fetchMergeRequests());
  };

  const onPress = (id: number) => {
    dispatch(mergeRequestActions.setSelectedMergeRequest(id));
    history.push('merge-request');
  };

  return (
    <FlatList
      style={styles.container}
      data={mergeRequests}
      keyExtractor={item => item.iid.toString()}
      renderItem={({ item, index }) => (
        <MergeRequestSummary
          mr={item}
          noBorder={!(index < mergeRequests.length - 1)}
          onPress={onPress}
        />
      )}
      refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
});
