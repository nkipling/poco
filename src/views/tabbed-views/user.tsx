import React from 'react';
import { StyleSheet, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { Button } from '../../components';
import { mergeRequestActions, signOutUser } from '../../store/actions';

export const User: React.FC = () => {
  const dispatch = useDispatch();

  const onLogoutPress = () => {
    dispatch(signOutUser());
  };

  const resetMrsPress = () => {
    dispatch(mergeRequestActions.setMergeRequests([]));
  };

  return (
    <View style={styles.container}>
      <Button type="primary" title="Sign out" onPress={onLogoutPress}></Button>
      <Button type="primary" title="Reset MRs" onPress={resetMrsPress}></Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
});
