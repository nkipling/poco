import React from 'react';
import { Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-native';
import { Button, HeaderBar, StatusBarBg } from '../components';
import { startUserSignIn } from '../store/actions';
import { Brand, Colors } from '../styles';

export const Welcome: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  // const loggedIn = useSelector(getLoggedIn);

  const signIn = async () => {
    dispatch(startUserSignIn());
  };

  const findOutMore = () => {
    history.push('/more-information');
  };

  const instanceSettings = () => {
    history.push('/instance-settings');
  };

  return (
    <>
      <StatusBarBg barStyle="light-content" backgroundColor={Brand.PurpleLight} />
      <SafeAreaView style={styles.flexGrow}>
        <HeaderBar title="" showSettings settingsPress={instanceSettings} />
        <ScrollView bounces={false} contentContainerStyle={styles.flexGrow}>
          <View style={styles.logoContainer}>
            <Image source={require('../assets/images/gitlab-icon-rgb.png')} style={styles.logo} />
            <Text style={styles.logoText}>Poco</Text>
            <Text style={styles.welcomeText}>
              A GitLab client that allows you to view issues, merge requests, todos and more on the
              go.
            </Text>
          </View>
          <View style={{ justifyContent: 'flex-end', padding: 32, flex: 1 }}>
            <Button type="secondary" title="Sign In" onPress={signIn} />
            <Button type="primary" title="Find out more" onPress={findOutMore} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  logo: {
    height: wp('40%'),
    width: wp('40%'),
  },
  logoText: {
    fontSize: 32,
    color: Colors.White,
  },
  flexGrow: {
    backgroundColor: Brand.PurpleLight,
    flexGrow: 1,
  },
  welcomeText: {
    textAlign: 'center',
    fontSize: 14,
    marginVertical: 16,
    marginHorizontal: 48,
    color: Colors.White,
    lineHeight: 28,
  },
});
